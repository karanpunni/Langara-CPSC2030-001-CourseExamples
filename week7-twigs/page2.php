<?php
//Once again, we use the same setup code
require_once 'sqlhelper.php';
require_once './vendor/autoload.php';  //include the twig library.
$twig = setupMyTwigEnvironment(); //moved twig setup code to it's own function, makes code more readable
$conn = connectToMyDatabase();

$store = $_GET["store"];
$result = $conn->query("call merch_table(\"$store\")");

if($result){
    $catalog = $result->fetch_all(MYSQLI_ASSOC); 
    $template = $twig->load("detailspage.twig.html");
    echo $template->render(array("catalog"=>$catalog, "store"=>$store));

} else {
    dumpErrorPage($twig);
}

?>